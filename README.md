# gladys-usb-sensor

gladys-usb-sensor

A simple code to get temp/humidity/air quality into Gladys via USB port.

More info on Gladys project by Jean-Marie Leymarie: https://gladysassistant.com/