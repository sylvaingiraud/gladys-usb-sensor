// Sylvain Giraud – Dec 2020
// Usage of Domoticz serial protocol

// MySensors serial protocol:
  // node-id;child-sensor-id;message-type;ack;sub-type;payload\n

  // Protocole MYSENSORS Serial.println("1;1;0;0;17;Arduino UNO\n");
  //1-Adresse du nœud sur le réseau mysensors
  //1-Numéro du capteur sur le nœud.Un nœud peut avoir plusieurs capteur température, humidité...etc
  //0-Type de Message (0,1,2,3 ou 4)
  //0-Ack pas pris en charge sur Domoticz????
  //17-types de sensors (ARDUINO pour le 17)


//#include <MySensors.h>

// What sensors are connected ?
// ( Do not connect Si7021 and DHT21 at same time)
#define SI7021_Sensor
//#define DHT_Sensor
#define MQ135_Sensor

// For test only. Do not plug Domoticz if enabled.
#define DEBUG false
#define INFO false


/* **************  Si7021    **************** */
#ifdef SI7021_Sensor
// Si_7021_bare
// this sketch reads temperature and relative humidity from a SI7021 I2C sensor
// sketch reports to Serial Monitor
// Floris Wouterlood – February 19, 2018
// public domain
// library
#include <Wire.h>

// declared variables for Si7021
const int ADDR = 0x40;    // Connected A4=SDA / A5=SCL
// SGI: fix unsigned instead of int, otherwise invalid if T>~40deg
unsigned X0,X1,Y0,Y1,Y2,Y3;
float X,Y,X_out,Y_out1,Y_out2;
#endif
/* ************* End of Si7021 ************* */

/*  ************* MQ135 Config **************** */
#ifdef MQ135_Sensor
/*
  Programme de test du MQ135 permettant de mesurer la présence de polluants dans l'atmosphère (CO, CO2 Alcool, fumées...)
  Pour plus d'infos ici http://www.projetsdiy.fr/mq135-mesure-qualite-air-polluant-arduino
  Utilise la librairie mq135.cpp mise à disposition par Georg Krocker https://github.com/GeorgK/MQ135
  Projets DIY - Mars 2016 - www.projetsdiy.fr
*/ 
#include "MQ135.h"
const int mq135Pin = 0;             // Pin sur lequel est branché de MQ135 : Analog 0 = A0 
MQ135 gasSensor = MQ135(mq135Pin);  // Initialise l'objet MQ135 sur le Pin spécifié
#endif
/*  ************* End of MQ135 Config **************** */

/*  ************* DHT Config **************** */
#ifdef DHT_Sensor
#include <DHT.h>
#define DHTPIN 2     // what pin we're connected to

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor
DHT dht(DHTPIN, DHTTYPE);
#endif
/*  ************* End of DHT Config **************** */

// Make global variables to test changes between periods, if needed
// List of sensors measures variables:
float h = 50.0;
float t = 20.0;
float ppm = 0;

struct sensorData {       // Création du type "sensorData" pour enregistrer les mesures.
  int id;
  float valeur;
};

#define NODEID 1  // id du devicetype de domoticz "température"
// id du devicetype de domoticz "température"
#define DEVICETYPE_TEMPERATURE 2
// id du devicetype de domoticz "Humidité"
#define DEVICETYPE_HUMIDITY    3
// id du devicetype de domoticz "MQ135"
#define DEVICETYPE_QUALITY     4

void setup() {
  Serial.begin(115200);

  /* **************  Si7021    **************** */
#ifdef SI7021_Sensor
  Wire.begin ();
  delay(1);
  readAir();
#endif
/* ************* End of Si7021 ************* */
  
//  dht.begin();        // Initialisation de l'objet dht

  // Need t and h to get CorrectedR0 of MQ135 after 24 hours
#ifdef MQ135_Sensor  
  //MQ135 calibration
  float rzero = 0;
  if (INFO) 
     {
      Serial.print(F("Compile R0: ")); Serial.println(RZERO);
      if (isnan(t) || isnan(h))
         {
         rzero = gasSensor.getRZero();
         Serial.print(F("R0: "));
         }
      else
         {
         rzero = gasSensor.getCorrectedRZero(t,h);
         Serial.print(F("CorrectedR0: "));
         }
     Serial.println(rzero);  // Valeur à reporter ligne 27 du fichier mq135.h après 48h de préchauffage
     }  
     //atmopi 19/07/2019 : CorrectedR0: 584.44

#endif

  delay(5000);

  // Presentation
  // node-id;child-sensor-id;presentation=0;ack;type;payload\n
  
  Serial.print(NODEID);Serial.println(";1;0;0;17;Arduino UNO\n");
  Serial.print(NODEID);Serial.print(";");Serial.print(DEVICETYPE_TEMPERATURE);Serial.println(";0;0;6;Temperature\n");
  Serial.print(NODEID);Serial.print(";");Serial.print(DEVICETYPE_HUMIDITY);   Serial.println(";0;0;7;Humidity\n");
  Serial.print(NODEID);Serial.print(";");Serial.print(DEVICETYPE_QUALITY);    Serial.println(";0;0;22;Air Quality\n");
  delay(10000);
  }

void loop() {

/* **************  Si7021    **************** */
#ifdef SI7021_Sensor
  readAir();
#endif
/* ************* End of Si7021 ************* */

/*  ************* DHT read **************** */
#ifdef DHT_Sensor
  // Lecture du taux d'humidité
  float h = dht.readHumidity();
  // Lecture de la température en °C
  float t = dht.readTemperature();
  // Pour lire la température en farenheit
  //float f = 73;  //dht.readTemperature(true);
#endif
/*  ************* End of DHT read **************** */

  // Stop le programme et renvoie un message d'erreur si le capteur ne renvoie aucune mesure
  if (isnan(h) || isnan(t)) {
    if (DEBUG) {Serial.println("Echec de lecture !");}
  } else {

     // node-id;child-sensor-id;set=1;ack;type;payload\n
     Serial.print(NODEID);Serial.print(";");Serial.print(DEVICETYPE_TEMPERATURE);Serial.print(";1;0;0;"); Serial.println(t,1);
     Serial.print(NODEID);Serial.print(";");Serial.print(DEVICETYPE_HUMIDITY);   Serial.print(";1;0;1;"); Serial.println(h,1);
     Serial.print(NODEID);Serial.print(";");Serial.print(DEVICETYPE_QUALITY);    Serial.print(";1;0;37;");Serial.println(ppm,1);
  }
  
  // 5 min polling
  delay(300000);
}



// Version Si7021 + MQ135
void readAir()
{
    // start temperature measurement
    Wire.beginTransmission (ADDR);
    Wire.write (0xE3);
    Wire.endTransmission ();
    
    // read temperature
    Wire.requestFrom (ADDR,2);
    
    if(Wire.available ()<=2);
    {
    X0 = Wire.read ();
    X1 = Wire.read ();
    X0 = X0<<8;
    X_out = X0+X1;
    }
    
    // SGI fix: sudden wrong value, ex: X0=65280;X1=65535;X=128.18
    //           keep X unchanged if happen
    if (X1 != 65535) 
         {
         X=(175.72*X_out)/65536;
         // SGI: fix measure. Substract 4deg.
         X=X-46.85-4;
         }
    
    // start relative humidity measurement
    Wire.beginTransmission (ADDR);
    Wire.write (0xE5);
    Wire.endTransmission ();
    
    // read relative humidity data
    Wire.requestFrom (ADDR,2);
    if(Wire.available()<=2);
    {
    Y0 = Wire.read ();
    Y2 = Y0/100;
    Y0 = Y0%100;
    Y1 = Wire.read ();
    Y_out1 = Y2*25600;
    Y_out2 = Y0*256+Y1;
    }
    
    // calculate relative humidity
    Y_out1 = (125*Y_out1)/65536;
    Y_out2 = (125*Y_out2)/65536;
    Y = Y_out1+Y_out2;
    Y = Y-6;

    // Push results to global variables
    t = X; h = Y;   
     
#ifdef MQ135_Sensor
    //float ppm = gasSensor.getPPM();
    // SGi correction with temperature and humidity
    ppm = gasSensor.getCorrectedPPM(h,t);
#endif

    if (DEBUG) 
       {
        // send temperature and humidity to Serial Monitor
        Serial.print ("t=");
        Serial.print (X,1);
        Serial.print ("C;");
        Serial.print ("h=");
        Serial.print (Y,1);
        Serial.print ("%;");
        Serial.print (ppm,1);
        Serial.print ("ppm;");
       }

}
// End of version Si7021
